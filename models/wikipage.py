from mongoengine import *
from models.metadata import Metadata

class WikiPage(Document):

	title = StringField(required=True)
	text = StringField()
	metadata = EmbeddedDocumentField(Metadata)