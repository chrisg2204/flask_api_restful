from mongoengine import *

class Metadata(EmbeddedDocument):

	tags = StringField()
	revisions = StringField()