import config
import logging

appConf = config.app
appConfApi = config.app.api

# Controllers
from controllers.hello_controller import Hello
from controllers.wiki_controller import WikiController

appConfApi.add_resource(Hello, '/')
appConfApi.add_resource(WikiController, '/wiki')

if __name__ == '__main__':
	# set up logging to file
	logging.basicConfig(
		filename='./log/access.log',
		level=logging.INFO, 
		format= '[%(asctime)s] - %(message)s',
		datefmt='%H:%M:%S'
	)
	
	console = logging.StreamHandler()
	formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
	console.setFormatter(formatter)
	logging.getLogger('').addHandler(console)
	logger = logging.getLogger(__name__)
	config.app.run(debug=True)
	# For Debug
	# config.app.run(use_debugger=False)