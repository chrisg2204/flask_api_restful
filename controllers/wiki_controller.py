from flask_restful import Resource, reqparse
# Models
from models.wikipage import WikiPage
from models.metadata import Metadata
import config
import logging

# DEBUG
from inspect import getmembers
from pprint import pprint

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

class WikiController(Resource):

	def post(self):
		
		# import wdb
		# wdb.set_trace()
		
		response = {}
		statusCode = None

		parser = reqparse.RequestParser()
		parser.add_argument('title', type=str, required=True, help='Title is required')
		parser.add_argument('text', type=str, required=True, help='Text is required')
		parser.add_argument('tags', type=str)
		parser.add_argument('revisions', type=str)
		
		args = parser.parse_args()

		logger.debug("Process data {}".format(args))

		try:

			meta = Metadata(
				tags= args["tags"],
				revisions= args["revisions"]
			)

			wikiPage = WikiPage(
				title=args["title"],
				text=args["text"],
			)

			isSaved = wikiPage.save()
			if isSaved.id:
				statusCode = 200
				response = {
					"statusCode": statusCode,
					"data": {
						"message": "Wiki {} created successfully".format(isSaved.id)
						}
				}

		except Exception as e:
			raise e
		
		return response, statusCode