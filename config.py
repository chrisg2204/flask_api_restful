import os
from flask import Flask
from flask_restful import Api
from flask_mongoengine import MongoEngine
# For Debug
# from wdb.ext import WdbMiddleware

app = Flask(__name__)
# For Debug
# app.debug = True
# app.wsgi_app = WdbMiddleware(app.wsgi_app)
app.config["MONGODB_SETTINGS"] = {
	"db": "mongoengine_test",
	"host": "172.20.0.2",
	"port": 27017
}
app.config["TESTING"] = True
db = MongoEngine()
db.init_app(app)
app.api = Api(app)